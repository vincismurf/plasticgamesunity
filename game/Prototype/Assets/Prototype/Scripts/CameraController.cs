﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {
    private RunnerController_B player;
    private Vector3 lastPlayerPos;
    private float distanceToMove;

	// Use this for initialization
	void Start () {
        player = FindObjectOfType<RunnerController_B>();
        lastPlayerPos = player.transform.position;
    }

    // Update is called once per frame
    void Update () {
        Vector3 pos = player.transform.position;
        distanceToMove = pos.x - lastPlayerPos.x;
        lastPlayerPos = player.transform.position;
        Vector3 deltaPos = new Vector3(distanceToMove, 0, 0);
        transform.position += deltaPos; 
    }
}
