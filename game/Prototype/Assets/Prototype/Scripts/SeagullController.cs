﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SeagullController : PooledObject
{
    public float moveSpeed;
    public float riseSpeed;
    public float maxHeight;
    public GameObject targetIcon;
    public float deathSeconds;
    public float minDeathSpin, maxDeathSpin;
    public float explodeSeconds;

    private Rigidbody2D rb;
    private Animator myAnimator;
    private GameObject aimStop;
    private float startGrav;
    private int deathState;

    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        myAnimator = GetComponent<Animator>();
        aimStop = GameObject.Find("AimStopPoint");
        startGrav = rb.gravityScale;
        rb.gravityScale = 0;
    }

    void Start () {
        initState();
    }

    void initState()
    {
        setDeathState(0);
    }

    // called when resurected by object pooler
    public override void OnActivated()
    {
        initState();
    }

    // FixedUpdate is called once per physics tick
    void FixedUpdate () {

        if (deathState == 0)
        {
            // if we have lagged behind the AimStop point, then turn off our target icon
            if (rb.position.x < aimStop.transform.position.x)
                targetIcon.SetActive(false);

            float rise = riseSpeed;
            if (rb.position.y >= maxHeight)
                rise = 0;
            rb.velocity = new Vector2(moveSpeed, rise);
        }
    }

    private void OnMouseDown()
    {
        processTouch();
    }

    private void processTouch()
    {
        // shoot a beer bottle at this critter
        if (targetIcon.activeSelf)
        {
            targetIcon.SetActive(false);
            GetComponent<IPlayer_B>().player.touchedSeagull(this);
        }
    }

    public void setDeathState(int value)
    {
        if (deathState != value)
        {
            deathState = value;
            myAnimator.SetInteger("DeathState", deathState);

            if (deathState == 1)
            {
                // now let gravity pull us down
                //rb.velocity = new Vector2(0, 0);
                rb.gravityScale = startGrav;
                float deltaSpin = maxDeathSpin - minDeathSpin;
                float rv = UnityEngine.Random.Range(0, deltaSpin);
                if (UnityEngine.Random.value <= 0.5f)
                    rv += minDeathSpin;
                else
                    rv = -rv - minDeathSpin;
                rb.angularVelocity = rv;

                // explode us in a bit?
                Invoke("explodeMe", deathSeconds);
            }
            else if (deathState == 2)
            {
                Invoke("setInactive", explodeSeconds);
            }
            else
            {
                rb.gravityScale = 0;
                rb.angularVelocity = 0;
            }
        }
    }

    public bool isAlive()
    {
        return deathState == 0;
    }

    public void killMe()
    {
        setDeathState(1);
    }

    private void explodeMe()
    {
        setDeathState(2);
    }

    private void setInactive()
    {
        rb.gravityScale = 0;
        rb.angularVelocity = 0;
        ObjectPooler.SetActive(gameObject, false);
    }
}
