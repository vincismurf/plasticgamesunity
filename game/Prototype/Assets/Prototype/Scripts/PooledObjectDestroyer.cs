﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PooledObjectDestroyer
    : MonoBehaviour {

    public GameObject destructionPoint;

	// Use this for initialization
	void Start () {
        if (destructionPoint == null)
            destructionPoint = GameObject.Find("DestructionPoint");
    }
	
	// FixedUpdate is called once per physics tick
	void FixedUpdate ()
    {
        if (transform.position.x < destructionPoint.transform.position.x)
            ObjectPooler.SetActive(gameObject, false);
    }
}
