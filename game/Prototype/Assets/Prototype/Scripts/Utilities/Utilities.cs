﻿using UnityEngine;
/// <summary>
/// 
/// </summary>
public static class Utilities  {

    /// <summary>
    /// 
    /// </summary>
    /// <param name="value"></param>
    /// <param name="from1"></param>
    /// <param name="to1"></param>
    /// <param name="from2"></param>
    /// <param name="to2"></param>
    /// <returns>float</returns>
    public static float Remap(this float value, float from1, float to1, float from2, float to2)
    {
        return (value - from1) / (to1 - from1) * (to2 - from2) + from2;
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="timer"></param>
    /// <returns>string</returns>
    public static string ConvertFloatToTime(float timer)
    {
        string minutes = Mathf.Floor(timer / 60).ToString("00");
        string seconds = Mathf.Floor(timer % 60).ToString("00");
        return minutes + ":" + seconds;
    }
    public static float ReturnMinutes(float timer)
    {
        return Mathf.Floor(timer / 60);
    }
    public static float ReturnSeconds(float timer)
    {
        return Mathf.Floor(timer % 60);
    }
}
