﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPooler : MonoBehaviour {

    public GameObject pooledObject;
    public int pooledAmount;

    List<GameObject> pooledObjects;

	// Use this for initialization
	void Start () {
        pooledObjects = new List<GameObject>();
        for (int i=0; i<pooledAmount; i++)
        {
            GameObject obj = Instantiate(pooledObject);
            obj.SetActive(false);
            pooledObjects.Add(obj);
        }
    }
	
    // get a pooled object
    public GameObject GetPooledObject()
    {
        foreach (GameObject obj in pooledObjects)
        {
            if (!obj.activeInHierarchy)
            {
                // do we activate here?
                return obj;
            }
        }

        GameObject newObj = Instantiate(pooledObject);
        SetActive(newObj, false);
        pooledObjects.Add(newObj);
        return newObj;
    }

    // make this object and all progeny active...
    public static void SetActive(GameObject go, bool active)
    {
        go.SetActive(active);
        foreach (Transform child in go.transform)
            SetActive(child.gameObject, active);

        if (active)
        {
            PooledObject po = go.GetComponent<PooledObject>();
            if (po != null)
                po.OnActivated();
        }
    }
}
