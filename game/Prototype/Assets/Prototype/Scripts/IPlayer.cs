﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IPlayer : MonoBehaviour {

    public RunnerController player;

    // Use this for initialization
    void Awake () {
        if (player == null)
            player = GameObject.Find("Player").GetComponent<RunnerController>();
    }
}
