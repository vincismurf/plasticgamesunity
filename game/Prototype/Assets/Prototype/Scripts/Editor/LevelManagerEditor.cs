﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(LevelManager))]
public class LevelManagerEditor : Editor{

    private LevelManager levelManager = null;
    private void Awake()
    {
        levelManager = (LevelManager)target;
    }

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        if (levelManager)
        {
            if (GUILayout.Button("Build Level"))
            {
                levelManager.BuildLevel();
            }
            if (GUILayout.Button("Loop Level"))
            {
                levelManager.LoopLevel();
            }
            if (GUILayout.Button("Reset Level"))
            {
                levelManager.ResetLevel();
            }
        }
    }
    /// <summary>
    /// 
    /// </summary>
    private void Update()
    {
       // Debug.Log("LevelManagerEditor::Update");
       if(levelManager)
        {
            if (levelManager.editorUpdate)
            {
                levelManager.EditorUpdate();
            }
        }
    }

    /// <summary>
    /// Sneaky Hack to get update in Editor
    /// </summary>
    private void OnEnable()
    {
        EditorApplication.update += Update;
    }
}
