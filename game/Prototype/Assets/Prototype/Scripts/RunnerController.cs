﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RunnerController : MonoBehaviour
{
	public float moveSpeed;
	public float jumpForce;
    public float slideTime;
    public LayerMask whatIsGround;
    public bool grounded;
    public bool sliding;
    public Vector2 beerSpawnOffset;
    public BeerBottleController beerBottle;

    private Rigidbody2D rb;
	private BoxCollider2D myCollider;
	private Animator myAnimator;
    private float slidingStartTime; // time we started sliding

	// Use this for initialization
	void Start()
	{
		rb = GetComponent<Rigidbody2D>();
		myCollider = GetComponent<BoxCollider2D>();
		myAnimator = GetComponent<Animator>();
	}

	// Update is called once per frame
	void Update()
	{
		grounded = Physics2D.IsTouchingLayers(myCollider, whatIsGround);
		rb.velocity = new Vector2(moveSpeed, rb.velocity.y);
		if (Input.GetKeyDown(KeyCode.Space) ||
			Input.GetMouseButtonDown(1))
		{
			if (!sliding && grounded)
			{
				rb.velocity = new Vector2(rb.velocity.x, jumpForce);
			}
		} else if (Input.GetKeyDown(KeyCode.D) ||
           Input.GetMouseButtonDown(2))
        {
            if (!sliding && grounded)
            {
                // go into slide animation
                // and change the hitbox
                slidingStartTime = Time.time;
                sliding = true;
                myCollider.offset = new Vector2(0, 0.4f);
                myCollider.size = new Vector2(1.171875f, 0.8125f);
            }
        }
        if (sliding)
        {
            float delta = Time.time - slidingStartTime;
            if (delta >= slideTime)
            {
                // not sliding - hit box back to normal...
                sliding = false;
                myCollider.offset = new Vector2(0, 0.6f);
                myCollider.size = new Vector2(0.8125f, 1.171875f);
            }
        }
		myAnimator.SetFloat("Speed", rb.velocity.x);
        myAnimator.SetBool("Grounded", grounded);
        myAnimator.SetBool("Sliding", sliding);
    }

    public void touchedSeagull(SeagullController seagull)
    {
        // launch beer bottle at seagull
        Vector2 pos = rb.position + beerSpawnOffset;
        BeerBottleController bbc = Instantiate<BeerBottleController>(beerBottle, pos, Quaternion.Euler(0, 0, 0));
        bbc.target = seagull.gameObject;
    }
}
