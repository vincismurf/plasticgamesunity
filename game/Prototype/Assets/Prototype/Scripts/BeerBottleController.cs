﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeerBottleController : MonoBehaviour {

    public float speed;
    public float minSpin, maxSpin;
    public Vector2 targetOffset; // point offset from target generated to get a curve
    public float failSafeDistance; // any closer than this we turn right AT the target
    public bool usePrediction; // user prediction for aiming at point
    public bool useAngles; // use angle interpolation to get a curve
    public float useAnglesTurnSpeed; // degrees per second allowed when using angles
    public float useAnglesBreakAngle; // at what angle do we just head right for the thing...
    public bool useBezier; // use bezier to get a curve
    public bool useBezierForPhysics;
    public float brokenSeconds;

    public GameObject target;

    private DebugShape dbs;
    private Rigidbody2D rb;
    private Animator myAnimator;
    private Rigidbody2D targetRB;
    private float startTime;
    private Vector2 startPoint;
    private Vector2 offsetPoint;
    private Vector2 predictedImpactPoint;
    private float estimatedSecondsToImpact;
    private bool launched = false;
    private bool broken;

    // Use this for initialization
    void Start () {

        dbs = GameObject.Find("DebugShape").GetComponent<DebugShape>();
        rb = GetComponent<Rigidbody2D>();
        myAnimator = GetComponent<Animator>();
        targetRB = target.GetComponent<Rigidbody2D>();
	}

    private void Update()
    {
        if (!launched)
        {
            launched = true;
            startTime = Time.time;
            startPoint = rb.position;

            float startDist = Vector2.Distance(startPoint, targetRB.position);
            estimatedSecondsToImpact = startDist / speed;
            estimatedSecondsToImpact += (estimatedSecondsToImpact * targetRB.velocity.magnitude) / speed;

            dbs.showX(targetRB.position, Color.magenta);

            // predict where target will be at converge time...
            predictedImpactPoint = targetRB.position + targetRB.velocity * estimatedSecondsToImpact;

            dbs.showPlus(predictedImpactPoint, Color.green);

            // aim at a given offset to that...
            offsetPoint = predictedImpactPoint + targetOffset;

            dbs.showCircle(offsetPoint, Color.red);

            // start off headed toward offset point...
            if (!useBezier)
            {
                Vector2 d = offsetPoint - startPoint;
                rb.velocity = d.normalized * speed;
            }

            // this thing spins as it goes...
            rb.angularVelocity = Random.Range(minSpin, maxSpin);
        }

        if (!broken && useBezier && !useBezierForPhysics)
        {
            float elapsed = Time.time - startTime;
            float fraction = elapsed / estimatedSecondsToImpact;
            if (fraction > 1)
            {
                destroySelfAndTarget();
            }
            else
            {
                Vector2 pos = CalculateQuadraticBezierPoint(fraction, startPoint, offsetPoint, predictedImpactPoint);
                transform.position = pos;
                if (dbs.showingShapes)
                    dbs.showCircle(pos, Color.blue);
            }
        }
    }

    void FixedUpdate()
    {
        if (!broken && launched && (!useBezier || useBezierForPhysics))
        {
            // what is pointed right AT target?
            float elapsed = Time.time - startTime;
            float fraction = elapsed / estimatedSecondsToImpact;
            Vector2 targetPos = usePrediction ? predictedImpactPoint : targetRB.position;
            Vector2 delta = targetPos - rb.position;
            float dist = delta.magnitude;
            Vector2 dir;
            if (fraction >= 1 || dist <= failSafeDistance)
            {
                // if we are half second past estimated time then just call it done!
                if (elapsed > estimatedSecondsToImpact + 0.5)
                {
                    destroySelfAndTarget();
                    return;
                }
                else
                {
                    // we are too close point right at it...
                    if (dist < 0.001)
                        dir = rb.velocity.normalized;
                    else
                        dir = delta / dist;
                }
            }
            else
            {
                if (useBezierForPhysics)
                {
                    float aheadFraction = fraction + 0.01f;
                    if (aheadFraction < 1)
                    {
                        Vector2 pos = CalculateQuadraticBezierPoint(fraction, startPoint, offsetPoint, predictedImpactPoint) - rb.position;
                        dir = pos.normalized;
                    }
                    else
                    {
                        dir = delta / dist;
                    }
                }
                else if (useAngles)
                {
                    // turn toward it by allowed increment...
                    Vector2 currDir = rb.velocity.normalized;
                    Vector2 targetDir = delta / dist;

                    float currRad = Mathf.Atan2(currDir.y, currDir.x);
                    float targetRad = Mathf.Atan2(targetDir.y, targetDir.x);

                    float currAngle = currRad * 180.0f / Mathf.PI;
                    float targetAngle = targetRad * 180.0f / Mathf.PI;

                    float deltaAngle = targetAngle - currAngle;
                    if (Mathf.Abs(deltaAngle) > useAnglesBreakAngle) // once less than this we head right at it...
                    {
                        // rotate by increment...
                        float inc = deltaAngle < 0 ? -useAnglesTurnSpeed : useAnglesTurnSpeed; // degrees per sec
                        inc *= Time.fixedDeltaTime;
                        float nextAngle = currAngle + inc;

                        float nextRad = nextAngle * Mathf.PI / 180.0f;
                        float x = Mathf.Cos(nextRad);
                        float y = Mathf.Sin(nextRad);
                        dir = new Vector2(x, y);
                    }
                    else
                    {
                        dir = delta / dist;
                    }
                }
                else
                {
                    dir = delta / dist;
                }
            }
            rb.velocity = dir * speed;

            if (dbs.showingShapes)
                dbs.showCircle(rb.position, Color.blue);
        }
    }

    public Vector2 CalculateQuadraticBezierPoint(float t, Vector2 p0, Vector2 p1, Vector2 p2)
    {
        float u = 1 - t;
        float tt = t * t;
        float uu = u * u;

        Vector2 p = uu * p0;
        p += 2 * u * t * p1;
        p += tt * p2;

        return p;
    }

    private void destroySelf()
    {
        Destroy(gameObject);
    }

    private void setBroken(bool value)
    {
        broken = value;
        myAnimator.SetBool("Broken", broken);

        if (broken)
        {
            Invoke("destroySelf", brokenSeconds);
        }
        else
        {
        }
    }

    private void destroySelfAndTarget()
    {
        // target is a pooled object
        SeagullController sc = target.GetComponent<SeagullController>();
        if (sc != null)
            sc.killMe();
        else
            ObjectPooler.SetActive(target, false);

        setBroken(true);
    }

    private bool processPossibleCollision(GameObject go)
    {
        if (go.Equals(target))
        {
            destroySelfAndTarget();
            return true;
        }
        return false;
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        processPossibleCollision(collider.gameObject);
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        processPossibleCollision(collision.gameObject);
    }
}
