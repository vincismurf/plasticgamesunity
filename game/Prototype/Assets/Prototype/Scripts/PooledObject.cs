﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class PooledObject : MonoBehaviour {

    // called after object is "created" meaning activated
    public abstract void OnActivated();
}
