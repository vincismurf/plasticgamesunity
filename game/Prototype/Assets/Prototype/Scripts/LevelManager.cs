﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#region Support
/// <summary>
/// In case we need multiple modes
/// </summary>
[System.Serializable]
public enum LevelBuilderState
{
    Dynamic = 0,
    Static = 1
}
/// <summary>
/// Helpers for Section lookup
/// </summary>
public enum SectionType
{
    Normal = 0,
    Transitional = 1
}
/// <summary>
/// 
/// </summary>
public enum SpawnPrefabType
{
    template = 0,
    sectionExplicit = 1,
    sectionRandom = 2

}
/// <summary>
/// Container for Multiple Sections
/// </summary>
[System.Serializable]
public struct Section
{
    /// <summary>
    /// Used to populate Holders
    /// </summary>
    public GameObject[] prefabs;
    /// <summary>
    /// Used to Build a Level Explicitly
    /// </summary>
    public int[] indies;
    /// <summary>
    /// Helper for Dynamic building
    /// </summary>
    public SectionType type;
    /// <summary>
    /// Used in movement model
    /// </summary>
    public Vector3 startPosition;
    /// <summary>
    /// Used in movement model
    /// </summary>
    public int positionId;

}
#endregion
/// <summary>
/// <author>Anthony Rosebaum</author>
/// <date>7-29-17</date>
/// Dynamic Prefab Loader
///  Inital test are promising BUT using Animator does seem to incur a little overhead, will look at dynamic movement next
///  Currently a pool is created from a template prefab, the pool will move and manage prefab loading and cleanup
/// </summary>
public class LevelManager : MonoBehaviour {
    #region Vars
    #region Public Vars
    [Header("Sections")]
    public SpawnPrefabType spawnType = SpawnPrefabType.template;
    public Section  [] sections;

    [Header("General")]
    public float xOffset = 48.0f;
    public float prefabTimeOnScreen = 2;
    public LevelBuilderState currentState = LevelBuilderState.Dynamic;
    [Range(0,50)]
    public float levelSpeed = 10;

    [Header("Section Pool")]
    public int poolSize = 8;

    public GameObject templatePrefab = null;
    [Header("Read Only")]
    [ReadOnly]
    public bool editorUpdate = false;
    [ReadOnly]
    public GameObject pool = null;
    [ReadOnly]
    public GameObject[] holders;
    [ReadOnly]
    public Section[] sectionPool;

    #endregion
    #region Private Vars
    //private Vector3 exitPool;
    //private Vector3 spawnPool;
    private float lastDelta;
    //private Coroutine loopCoroutine = null;
    private int currentSection = 0;
    private int currentSectionPrefab = 0;
    #endregion
    #endregion
    #region Methods
    #region Unity Methods
    /// <summary>
    /// Use this for initialization
    /// </summary>
    void Start ()
    {
        BuildLevel();
    }
	/// <summary>
    ///  Update is called once per frame - Use for Input
    /// </summary>
    void Update ()
    {
    }
    /// <summary>
    ///  Use for Physics and Animator stuff
    /// </summary>
    private void FixedUpdate()
    {
        UpdateLoop(Time.fixedTime);
    }


    #region Editor Methods
    /// <summary>
    /// Used to Build a Pool in editor
    /// </summary>
    public void BuildLevel()
    {
        if(poolSize > 0 && pool == null)
        {
            CreatePool();
        }

        InitPool();
    }
    /// <summary>
    /// 
    /// </summary>
    public void LoopLevel()
    {
        if (pool)
        {
            // StartCoroutine(LoopPool());
            editorUpdate = !editorUpdate;
        }
        else
        {
            Debug.Log("LevelManager::LoopLevel ERROR - Pool is Null");
        }
    }
    /// <summary>
    /// 
    /// </summary>
    public void ResetLevel()
    {
        InitPool();
    }
    #endregion
    #endregion
    #region Pool Methods
    /// <summary>
    /// Create a GameObject Pool to hold each Item in the Pool
    /// </summary>
    private void CreatePool()
    {
        if (poolSize > 0)
        {
            sectionPool = new Section[poolSize];
            //idLookUp = new int[poolSize];
            //Debug.Log("Creating Pool");
            holders = new GameObject[poolSize];
            pool = new GameObject("Pool");
            pool.transform.parent = gameObject.transform;
            pool.transform.position = Vector3.zero;
            pool.transform.localScale = Vector3.one;

            //Init Holders
            for (int i = 0; i < holders.Length; i++)
            {
                sectionPool[i] = new Section();
                sectionPool[i].positionId = i;
                GameObject poolItem = new GameObject("Pool_Item_"+i.ToString());
                poolItem.transform.parent = pool.transform;
                holders[i] = poolItem;
            }

            //Set up range
            //exitPool = new Vector3(-xOffset * 1, 0, 0);
            //spawnPool = new Vector3(xOffset * (holders.Length-2), 0, 0);
        }
        else
        {
            Debug.Log("LevelManager::CreatePool ERROR - poolSize is " + poolSize);
        }
    }
    /// <summary>
    ///  Position and populate items in Pool
    /// </summary>
    private void InitPool()
    {
        currentSection = 0;
        currentSectionPrefab = 0;
        if (pool)
        {
            //Debug.Log("Initializing Pool");
            for (int i = 0; i < holders.Length; i++)
            {
                GameObject poolItem = holders[i];
                poolItem.transform.parent = pool.transform;
                poolItem.transform.localPosition = Vector3.zero + new Vector3(xOffset * i, 0, 0);
                poolItem.transform.localScale = Vector3.one;

                sectionPool[i].startPosition = poolItem.transform.localPosition;

                GameObject nextPrefab = GetPrefabFromSection();
                if (nextPrefab != null)
                {
                    GameObject prefab = (GameObject)Instantiate(nextPrefab);
                    prefab.transform.parent = poolItem.transform;
                    prefab.transform.localPosition = Vector3.zero;
                    prefab.transform.localScale = Vector3.one;
                }
            }
        }
        else
        {
            Debug.Log("LevelManager::InitPool ERROR - Pool is Null");
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    private GameObject GetPrefabFromSection()
    {
        GameObject prefab = null;

        switch(spawnType)
        {
            case SpawnPrefabType.template:
                prefab = spawnTemplate();   
                break;
            case SpawnPrefabType.sectionExplicit:
                prefab = spawnSectionExplicit();
                break;
            case SpawnPrefabType.sectionRandom:
                prefab = spawnSectionRandom();
                break;
        }


        return prefab;
    }
    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    private GameObject spawnTemplate()
    {
        GameObject prefab = null;
        if (templatePrefab)
        {
            prefab = templatePrefab;
        }
        return prefab;
    }
    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    private GameObject spawnSectionExplicit()
    {
        GameObject prefab = null;
        if (currentSection >= sections.Length)
        {
            currentSection = 0;
        }

        prefab = sections[currentSection].prefabs[currentSectionPrefab];

        currentSectionPrefab++;
        if (currentSectionPrefab >= sections[currentSection].prefabs.Length)
        {
            currentSectionPrefab = 0;
            currentSection++;
        }
        return prefab;
    }
    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    private GameObject spawnSectionRandom()
    {
        GameObject prefab = null;
        int randomSection = Random.Range(0, sections.Length - 1);
        int randomPrefab = Random.Range(0, sections[randomSection].prefabs.Length - 1);
        prefab = sections[randomSection].prefabs[randomPrefab];
        return prefab;
    }
    #endregion
    #region Looping Coroutine
    /// <summary>
    /// Used for Editor Testing
    /// </summary>
    public void EditorUpdate()
    {
        UpdateLoop(Time.time);
    }

    /// <summary>
    /// Loop logic, can be fed Time.time or Time.fixedUpdate
    /// </summary>
    /// <param name="time"></param>
    private void UpdateLoop(float time)
    {
        float delta = Mathf.Repeat(time * levelSpeed, xOffset );

        if (lastDelta > delta)
        {
            for (int i = 0; i < holders.Length; i++)
            {
                int id = sectionPool[i].positionId;

                id--;
                if (id < 0)
                {
                    id = holders.Length - 1;
                    GameObject nextPrefab = GetPrefabFromSection();
                    if (nextPrefab != null)
                    {
                        GameObject prefab = (GameObject)Instantiate(nextPrefab);
                        prefab.transform.parent = holders[i].transform;
                        prefab.transform.localPosition = Vector3.zero;
                        prefab.transform.localScale = Vector3.one;
                    }
                }
                sectionPool[i].positionId = id;
            }
        }


        for (int i = 0; i < holders.Length; i++)
        {
            int id =  sectionPool[i].positionId;
            holders[i].transform.localPosition = sectionPool[id].startPosition - Vector3.right * delta;
        }

        lastDelta = delta;
    }
    /// <summary>
    /// Empty Coroutine, template for later
    /// </summary>
    /// <returns></returns>
    IEnumerator LoopPool()
    {
        yield return new WaitForEndOfFrame();
    }
    #endregion
    #endregion
}
