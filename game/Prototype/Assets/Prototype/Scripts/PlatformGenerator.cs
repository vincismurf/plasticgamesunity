﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformGenerator : MonoBehaviour {

    public GameObject thePlatform;
    public GameObject theObjectPool;
    public Transform generationPoint;
    public float distanceBetweenMin;
    public float distanceBetweenMax;

    private float distanceBetween;
    private float platformWidth;

	// Use this for initialization
	void Start () {
        platformWidth = thePlatform.GetComponent<BoxCollider2D>().size.x;
	}
	
	// Update is called once per frame
	void Update () {

		if (transform.position.x < generationPoint.position.x)
        {
            distanceBetween = Random.Range(distanceBetweenMin, distanceBetweenMax);
            transform.position = new Vector3(transform.position.x + platformWidth + distanceBetween, transform.position.y, transform.position.z);

            GameObject obj = theObjectPool.GetComponent<ObjectPooler>().GetPooledObject();
            obj.transform.position = transform.position;
            obj.transform.rotation = transform.rotation;
            ObjectPooler.SetActive(obj, true);
        }
	}
}
