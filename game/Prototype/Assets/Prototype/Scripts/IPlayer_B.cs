﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IPlayer_B : MonoBehaviour {

    public RunnerController_B player;

    // Use this for initialization
    void Awake () {
        if (player == null)
            player = GameObject.Find("Player").GetComponent<RunnerController_B>();
    }
}
