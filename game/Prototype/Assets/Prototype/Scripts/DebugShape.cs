﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugShape : MonoBehaviour {

    public bool showingShapes;
    public GameObject debugPlus;
    public GameObject debugCircle;
    public GameObject debugX;

	// Use this for initialization
	void Start () {
    }

    public void showPlus(Vector2 pos, Color color)
    {
        showShape(pos, color, debugPlus);
    }

    public void showCircle(Vector2 pos, Color color)
    {
        showShape(pos, color, debugCircle);
    }

    public void showX(Vector2 pos, Color color)
    {
        showShape(pos, color, debugX);
    }

    public void showShape(Vector2 pos, Color color, GameObject prefab)
    {
        if (showingShapes)
        {
            GameObject go = Instantiate(prefab);
            go.SetActive(true);
            go.transform.position = pos;
            go.GetComponent<SpriteRenderer>().color = color;
        }
    }
}
