﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestGenerator : MonoBehaviour {

    public GameObject theObject;
    public GameObject theObjectPool;
    public Transform generationPoint;
    public float minGenerationPeriod, maxGenerationPeriod; // seconds between generation, 0 means don't generate at all...
    public Vector2 minOffset, maxOffset; // random offset for generation point...

    private RunnerController player;
    private float generationPeriod;
    private float lastGenerationTime;

	// Use this for initialization
	void Start () {
        lastGenerationTime = Time.time;
        player = GetComponent<IPlayer>().player;
        if (minGenerationPeriod > 0 && maxGenerationPeriod > 0)
            randomize();
    }

    private void randomize()
    {
        generationPeriod = Random.Range(minGenerationPeriod, maxGenerationPeriod);
    }

    // FixedUpdate is called once per physics tick
    void FixedUpdate () {

        if (generationPeriod > 0)
        {
            float now = Time.time;
            float elapsed = now - lastGenerationTime;
            if (elapsed >= generationPeriod)
            {
                lastGenerationTime = now;
                randomize();

                GameObject obj = theObjectPool.GetComponent<ObjectPooler>().GetPooledObject();
                IPlayer ip = obj.GetComponent<IPlayer>();
                if (ip != null)
                    ip.player = player;
                Vector2 pos = generationPoint.position;
                pos += new Vector2(Random.Range(minOffset.x, maxOffset.x), Random.Range(minOffset.y, maxOffset.y));
                obj.transform.position = pos;
                obj.transform.rotation = generationPoint.rotation;
                ObjectPooler.SetActive(obj, true);
            }
        }
	}
}
